<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>DH EXplained|Digi-Humanist</title>
<link rel="stylesheet" type="text/css" href="dhstyles.css">
</head>
<body>
	<div id="container">
		<header>
			<div id="logo">
				<img src="images/learndh.png" alt="learndh">
			</div>
			<h1>Digi-Humanist</h1>
			<h2>
				<em>Digital Humanities explained, all under one roof!</em>
			</h2>
		</header>
		<div id="navigation">
			<ul id="tabs">
				<li id="index"><a href="index.html">Home</a></li>
				<li id="dh explained"><a href="dhexplained.html">DH
						Explained</a></li>
				<li id="contact"><a href="contact.html">Contact</a></li>
				<li id="events"><a href="events.html">Events</a></li>
				<li id="resources"><a href="resources.html">Resources</a></li>
				<li id="socialmedia"><a href="socialmedia.html">Social
						Media</a></li>
			</ul>
		</div>
		<div id="primary2">
			<h2>Key Concepts</h2>
			<p>
				The Humanities discipline has undergone somewhat of an identity
				crisis in the past years. Many have turned to I.T. in this crisis of
				progression to create an image based on trend rather than
				functionality and understanding. I would like to propose that
				Digital Humanities forgoes style for substance in the application of
				I.T. to its mission.<br> Alan Liu deals with this issue in his
				work <i><a href="http://sms.cam.ac.uk/media/1173142">The
						State of the Digital Humanities- A report and a critique</a>, </i>of which
				there is also a video available of him delivering this paper. In
				this report, he satirises the obsession with the image of I.T.,
				which he refers to as an "institutional desiring engine" (2).<br>
			</p>
			<p>There is a domineering strand of cultural I.T. in which image
				is everything. These people are building their I.T. identity based
				upon trends, rather than acknowledging the impact of technology upon
				Modern research and innovation. These type of users can be referred
				to as mindless participators in I.T., whereas those who have entered
				into their work using digital means can be identified as mindful
				participators. Arguably, digital humanists fall into this category.</p>
			<p>
				They recognise the character and adaptability of the uses of I.T. in
				their work, unlike those mindless users who are mainly concerned
				with the appearance that they will gain from these 'cool' gadgets.
				It is my opinion that the digital world cannot reach its full
				potential if it remains obsessed with its own cultural image.<br>
				Now, you may ask, what does this theory have to do with the crisis
				of identity in the Humanities? The answer to this is everything.
				Humanities is also in danger of falling into the mindless
				participatory role, if old conventions on the uses of I.T. cannot be
				forgotten. The Humanities in general have been at a cross-roads for
				some time in terms of its image. William Pannapacker has discussed
				this attempt to re-define the Humanities in his work: <i><a
					href="http://chronicle.com/article/Big-Tent-Digital-Humanities-a/129036/">Big
						Tent Digital Humanities: A view from the edge, part 2</a>, </i>starting
				with its problems<br>
			</p>
			<blockquote class="tr_bq">"Some are related to the
				traditions of academic culture&#8212;the apparent disinclination of
				some humanists to work with digital technologies, the academic
				tendency to value individual achievement over teamwork, and the
				continuing emphasis on the use of scholarly monographs to certify
				tenure and promotion. Other challenges seem more structural, such as
				declining financial support for the humanities in general"
				(Pannapacker).</blockquote>
			<blockquote class="tr_bq">"As Bernard Stiegler suggests,
				technology is a kind of&nbsp;pharmakon, by which he means (via Plato
				and Derrida) that it is both a remedy&nbsp;and a poison.To know the
				difference, however, it is essential that we become&nbsp;computer
				literate and that universities support what Drucker calls
				&#8220;humanistic&nbsp;approaches&#8221; to manipulations of
				information, its visualization and its modeling" (120).</blockquote>
			This, for me, symbolises the fears and rewards which cross the minds
			of many a humanist when they debate the entry into the world of
			Digital Humanities. There is a fine line between these two types of
			I.T. identities, which can be beneficial or detrimental to the cause
			of preserving the Humanities. <br>
		</div>
		<div id="youtube">
			<iframe width="560" height="315"
				src="http://www.youtube.com/embed/IrvUys_STcs"></iframe>
		</div>
		<figure id="book">
			<img src="images/book.jpg" alt="book">
			<figcaption>
					<em>The Future . . . . . . . </em>
				</figcaption>
		</figure>
		<footer>
			<div class="license">
				<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img
					alt="Creative Commons License" style="border-width: 0"
					src="http://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />
				This work is licensed under a <a rel="license"
					href="http://creativecommons.org/licenses/by/4.0/">Creative
					Commons Attribution 4.0 International License</a>.
			</div>
		</footer>




	</div>


</body>
</html>